package spreadsheetUpdates.util;
import java.util.Scanner;



public class FileProcessor {
	private Scanner scanner = null;
	
	class FileEmptyException extends Exception {
		/**
		 * user defined exception
		 */
		private static final long serialVersionUID = 1L;

		public FileEmptyException(String msg) {
			super(msg);
		}
	}
	/**
	 * FileProcessor constructor
	 * @param scannerIn - scanner
	 */
	public FileProcessor(Scanner scannerIn) {
		scanner = scannerIn;
		if (!scanner.hasNextLine()) {
			try {
				throw new FileEmptyException("empty");
			} catch (FileEmptyException e) {
				System.err.println("Input file is empty...");
				System.exit(1);
			}
		}
		Logger.writeMessage("In FileProcessor constructor", Logger.DebugLevel.CONSTRUCTOR);
	}
	
	/**
	 * @return line read from the input file
	 */
	public String readLine(){
		String line = null;
		try {
			if (!scanner.hasNextLine()) {
				return null;
			}
			line = scanner.nextLine();
		}
		finally {
			//scanner.close();
		}
		return line;
	}
	@Override
	public String toString() {
		return "Scanner: "+scanner.toString();
	}
}
