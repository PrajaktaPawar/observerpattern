package spreadsheetUpdates.util;

/**
 * 
 * @author Prajakta Pawar
 * interface to write schedules to file.
 */
public interface FileDisplayInterface {
	void writeResultsToOutFile(String outputFileName);
}
