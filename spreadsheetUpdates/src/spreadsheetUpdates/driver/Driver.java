package spreadsheetUpdates.driver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import spreadsheetUpdates.util.Logger;
import spreadsheetUpdates.observer.Spreadsheet;
import spreadsheetUpdates.util.FileDisplayInterface;
import spreadsheetUpdates.util.FileProcessor;

public class Driver {

	public static void main(String[] args) {
		
		FileProcessor fileProcessorIn = null;
		FileDisplayInterface fileDisplayInterface = null;
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null;
		String outputFile = null;
		try {
			validateCommandLineArguments(args);
			int debug_level = Integer.parseInt(args[2]);
			Logger.setDebugValue(debug_level);
			fileProcessorIn = new FileProcessor(new Scanner(new File(args[0])));
			outputFile = args[1];
			try {
				fileWriter = new FileWriter(outputFile);
				bufferedWriter = new BufferedWriter(fileWriter);
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
			}
			fileDisplayInterface = new Spreadsheet(bufferedWriter,fileWriter,fileProcessorIn);
			((Spreadsheet)fileDisplayInterface).processFileData();
			
			fileDisplayInterface.writeResultsToOutFile(outputFile);
			
		} catch (FileNotFoundException e) {
			System.err.println("Input file not found..");
			e.printStackTrace();
		}
	}

	/**
	 * method to validate command line arguments
	 * 
	 * @param args - command line arguments
	 */
	private static void validateCommandLineArguments(String args[]) {
		// TODO Auto-generated method stub

		if (args.length != 3) {
			System.err.println("Some arguments are missing..");
			System.exit(0);
		}
		try {
			if((Integer.parseInt(args[2])) < 0 || (Integer.parseInt(args[2])) > 4){
				System.err.println("Debug Value should be between 0 and 4..");
				System.exit(0);
			}
		} catch (NumberFormatException e) {
			System.err.println("String Cannot be converted to integer..");
			e.printStackTrace();
			System.exit(0);
		}

	}
}
