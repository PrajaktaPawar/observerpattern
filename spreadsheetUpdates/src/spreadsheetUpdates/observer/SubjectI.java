package spreadsheetUpdates.observer;

/**
 * Interface to register,remove observers and notify observers with updated cell value.
 * @author Prajakta Pawar
 */
public interface SubjectI {

	public void registerObserver(ListenerI obserevr);
	public void removeObserver(ListenerI obserevr);
	public void notifyObservers();
}
