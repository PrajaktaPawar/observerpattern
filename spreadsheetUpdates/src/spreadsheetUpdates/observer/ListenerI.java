package spreadsheetUpdates.observer;

/**
 * Interface to send the updated value to observers through update method and update value.
 * @author Prajakta Pawar
 *
 */
public interface ListenerI {
	public void update();
}
