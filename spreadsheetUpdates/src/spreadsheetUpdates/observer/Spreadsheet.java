package spreadsheetUpdates.observer;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import spreadsheetUpdates.observer.Cell.CycleDetectedException;
import spreadsheetUpdates.util.FileDisplayInterface;
import spreadsheetUpdates.util.FileProcessor;
import spreadsheetUpdates.util.Logger;

public class Spreadsheet implements FileDisplayInterface{

	Map<String, Cell> cells = new HashMap<String, Cell>();
	BufferedWriter bufferedWriter = null;
	FileWriter fileWriter = null;
	FileProcessor fileProcessor=null;

	/**
	 * Spreadsheet constructor
	 * @param bufferedWriterIn BufferedWriter object
	 * @param fileWriterIn FileWriter object
	 * @param fileProcessorIn FileProcessor object
	 */
	public Spreadsheet(BufferedWriter bufferedWriterIn,FileWriter fileWriterIn,FileProcessor fileProcessorIn) {
		initializeCellsWithZero();
		bufferedWriter = bufferedWriterIn;
		fileWriter = fileWriterIn;
		fileProcessor = fileProcessorIn;
		Logger.writeMessage("In Spreadsheet constructor", Logger.DebugLevel.CONSTRUCTOR);
	}

	/**
	 * initialize all the cells with cell indexes referring to rows a-z, and columns 1-26.
	 */
	public void initializeCellsWithZero() {
		
		for (char i = 'a'; i <= 'z'; i++) {
			for (int j = 1; j <= 26; j++) {
				Cell cell = new Cell(String.valueOf(i).concat(String.valueOf(j)));
				cells.put(cell.getName(), cell);
				Logger.writeMessage("Cell "+ cell.toString(), Logger.DebugLevel.STORE_DATA_STRUCTURE);
			}
		}
	}

	/**
	 * separate operands from read line and set operands(subjects) to left cell(observer)
	 * @param line - line read from input file
	 */
	public void fillData(String line) {
		String[] cellNames = line.split("=");
		String afterEqual = cellNames[1];

		String[] operandsNames = afterEqual.split("\\+");

		Cell lhsCell = cells.get(cellNames[0]);
		Object[] operandsCells = new Object[2];

		try {
			Object operand1 = Integer.parseInt(operandsNames[0]);
			if(operand1 instanceof Integer && ((Integer) operand1).intValue() >= 10){
					operandsCells[0] = operand1;
			}else{
				throw new InvalidOperandException("operand1 should be greater than 10");
			}
		} catch (NumberFormatException e) {
			if(operandsNames[0] != null)
				operandsCells[0] = cells.get(operandsNames[0]);
		}catch(InvalidOperandException e){
			System.err.println("operand1 should be greater than 10"+line);
			//System.exit(1);
		}

		try {
			Object operand1 = Integer.parseInt(operandsNames[1]);
			if(operand1 instanceof Integer && ((Integer) operand1).intValue() >= 10){
				operandsCells[1] = operand1;
			}else{
				throw new InvalidOperandException("operand2 should be greater than 10");
			}
			
		} catch (NumberFormatException e) {
			if(operandsNames[1] != null)
				operandsCells[1] = cells.get(operandsNames[1]);
		}catch(InvalidOperandException e){
			System.err.println("operand2 should be greater than 10 in "+ line);
			//System.exit(1);
		}

		try {
			lhsCell.setOperands(operandsCells, line);
			lhsCell.update();
		} catch (CycleDetectedException e) {
			try {
				bufferedWriter.write("cycle detected!! Ignoring line : "+line+"\n");
				Logger.writeMessage("cycle detected!! Ignoring line : "+line+"\n", Logger.DebugLevel.RESULT_DATA_STRUCTURE);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}catch(NullPointerException e){
			System.err.println("Invalid cell in " + line);
			//System.exit(1);
		}

	}
	/**
	 * User defined exception thrown when input operands are < 10
	 */
	class InvalidOperandException extends Exception {
		/**
		 * user defined exception
		 */
		private static final long serialVersionUID = 1L;

		public InvalidOperandException(String msg) {
			super(msg);
		}
	}
	/**
	 * calculate final overall sum of all cells
	 * @return final result
	 */
	public Integer calculateOverallSum() {
		Integer result = 0;
		for (Map.Entry<String, Cell> entry : cells.entrySet()) {
			result += entry.getValue().getCellValue();
			Logger.writeMessage("Cell Info"+ entry.getValue().toString(), Logger.DebugLevel.STORE_DATA_STRUCTURE);
		}
		return result;
	}

	/**
	 * method to write results to output file.
	 * @param outputFile - output file to write cycle info and final sum
	 */
	public void writeResultsToOutFile(String outputFile) {

		try {
			Integer result = calculateOverallSum();
			bufferedWriter.write("\nThe sum of all cell values is: " + result);
			Logger.writeMessage("\nThe sum of all cell values is: " + result, Logger.DebugLevel.RESULT_DATA_STRUCTURE);
		} catch (FileNotFoundException ex) {
			System.err.println("Unable to open file.. " + outputFile + " Not found");
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bufferedWriter != null)
					bufferedWriter.close();

				if (fileWriter != null)
					fileWriter.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}
	}

	/**
	 * read line from input file and send line to fillData to process on it.
	 */
	public void processFileData() {
		String line = null;
		line = fileProcessor.readLine();
		while (line != null) {
			fillData(line);
		line = fileProcessor.readLine();
		}
	}
	
	@Override
	public String toString() {
		return "Cells Map :"+ Arrays.toString(cells.keySet().toArray());
	}
}
