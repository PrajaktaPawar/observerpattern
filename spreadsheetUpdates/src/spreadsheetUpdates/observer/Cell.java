package spreadsheetUpdates.observer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import spreadsheetUpdates.util.Logger;

public class Cell implements SubjectI, ListenerI {
	private List<ListenerI> observers;
	private String name;
	private Integer cellValue = 0;
	private Object[] operands = { null, null };

	/**
	 * Cell constructor
	 * @param nameIn cell name
	 */
	public Cell(String nameIn) {
		name = nameIn;
		observers = new ArrayList<ListenerI>();
		Logger.writeMessage("In Cell constructor", Logger.DebugLevel.CONSTRUCTOR);
	}

	/**
	 * Method to notify observers that cell value has changed and update cell value.
	 */
	@Override
	public void update() {
		cellValue = calculateSum();
		Logger.writeMessage("Cell " + this.getName() + ":" + cellValue, Logger.DebugLevel.CELL_VALUE);
		notifyObservers();
	}

	/**
	 * method to add observer into list
	 */
	@Override
	public void registerObserver(ListenerI obserevr) {
		observers.add(obserevr);
	}

	/**
	 * Method to remove observer from list
	 */
	@Override
	public void removeObserver(ListenerI obserevr) {
		int i = observers.indexOf(obserevr);
		if (0 <= i)
			observers.remove(i);
	}

	/**
	 * notify observers that cell value has been changed.
	 */
	@Override
	public void notifyObservers() {
		for (int i = 0; i < observers.size(); i++) {
			ListenerI o = observers.get(i);
			o.update();
		}
	}
	/**
	 * getter for cell name
	 * @return cell name
	 */
	public String getName() {
		return name;
	}

	/**
	 * getter for cell value
	 * @return cell value
	 */
	public Integer getCellValue() {
		return cellValue;
	}

	/**
	 * method to set operands to particular observer.
	 * @param operandsIn - operands in each line
	 * @param line - line read from file
	 * @throws CycleDetectedException - throws an exception when cycle is detected.
	 */
	public void setOperands(Object[] operandsIn, String line) throws CycleDetectedException {
		for (int i = 0; i < operandsIn.length; i++) {
			Object operand = operandsIn[i];

			// registration of observer
			try {
				Cell rhsCell = (Cell) operand; // expected exception - handled

				// Cycle check

				cycleCheck(rhsCell, this);

				rhsCell.registerObserver(this);

			} catch (ClassCastException e) {
				// e.printStackTrace();
				// rhs is integer
			}
			// check for old operand - removal of observer if needed
			try {
				Cell oldOperand = (Cell) operands[i]; // check if cell --
														// exception
				// call removeObserver
				if (oldOperand != null)
					oldOperand.removeObserver(this);
			} catch (ClassCastException e) {
				// e.printStackTrace();
			}
			operands[i] = operand;
		}
	}

	class CycleDetectedException extends Exception {
		/**
		 * user defined exception
		 */
		private static final long serialVersionUID = 1L;

		public CycleDetectedException(String msg) {
			super(msg);
		}
	}

	/**
	 * method to check if cycle is created or not
	 * @param rhsCell - subject
	 * @param currentCell - observer
	 * @throws CycleDetectedException - throws an exception when cycle is detected.
	 */
	public void cycleCheck(Cell rhsCell, Cell currentCell) throws CycleDetectedException {

		for (int j = 0; j < currentCell.observers.size(); j++) {
			ListenerI o = currentCell.observers.get(j);
			if (o.equals(rhsCell) || rhsCell.equals(currentCell)) {
				throw new CycleDetectedException("Cycle is detected..");
			} else if (!o.equals(rhsCell)) {
				cycleCheck(rhsCell, (Cell) o);
			}
		}
	}

	/** 
	 * calculate cell value by adding operands.
	 * @return cell value
	 */
	public int calculateSum() {
		int result = 0;
		int op = 0;
		for (int i = 0; i < operands.length; i++) {
			try {
				op = (Integer) operands[i];
			} catch (ClassCastException e) {
				op = ((Cell) operands[i]).getCellValue();
			}
			result += op;
		}
		return result;
	}
	
	@Override
	public String toString() {
		return " Cell Name: "+ name +
				" \n Cell Value :"+ cellValue + 
				" \n Operands : "+ Arrays.toString(operands); 
	}
}
