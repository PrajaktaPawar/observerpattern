Spreadsheet-Features
Application of design principles to implement a spreadsheet feature

Project Description: Observer Pattern used to implement the feature of a spreadsheet wherein values of cells get updated, when the cells they are dependent on get updated.

An example of the input.txt file:

a1=32+27 a2=a1+37 a3=a1+a2 b1=29+31 a1=37+b1

Each line of the input file will have a simple expression.

The value before the equals sign will always refer to the cell. The right hand side will always have two operands and the "+" operator.

The operands will be either two digit integers greater than 10 or cell indexes. The cell indexes will only refer to rows a-z, and columns 1-26.

DEBUG_VALUE=1 [Print to stdout the total cycles detected] DEBUG_VALUE=0 [Print to stdout The sum of all cell values is: X"] where X= sum of the cells

Assuming you are in the directory containing src folder


## To clean:
ant -buildfile src/build.xml -Darg0=input.txt -Darg1=output.txt -Darg2=0 clean

-----------------------------------------------------------------------
## To compile: 
ant -buildfile src/build.xml -Darg0=input.txt -Darg1=output.txt -Darg2=0

-----------------------------------------------------------------------
## To run by specifying arguments from command line 
## We will use this to run your code
ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=output.txt -Darg2=0

-----------------------------------------------------------------------

## To create tarball for submission
tar -cvf prajakta_pawar_assign_3.tar prajakta_pawar_assign_3/
gzip prajakta_pawar_assign_3.tar

## To untar the tarball:
tar -zxvf prajakta_pawar_assign_3.tar.gz



-----------------------------------------------------------------------

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.”

[Date: ] -- 04/02/2017

-----------------------------------------------------------------------

Provide justification for Data Structures used in this assignment in
term of Big O complexity (time and/or space)

HashMap - O(1) - To store cells data(Cell Name as key and Cell object as value). 
Hashmap works on principle of hashing and internally uses hashcode as a base, for storing key-value pair.
With the help of hashcode, Hashmap distribute the objects across the buckets in such a way that 
hashmap put the objects and retrieve it in constant time O(1).

DFS for cycle check.
Internally there is an adjacency List to store observers of the different cells.
checked observers of the cells recursively to find observer which may create cycle.
if found,cycle found.
if subject is itself an observer then also it will create cycle. checked that condition as well.
-----------------------------------------------------------------------

Provide list of citations (urls, etc.) from where you have taken code
(if any).

